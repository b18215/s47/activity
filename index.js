console.log('hi');

console.log(document);
//result: the document HTML code

const txtFirstName = document.querySelector("#txt-first-name");

console.log(txtFirstName);

const txtLastName = document.querySelector("#txt-last-name");

console.log(txtLastName);




/*
	alternative ways:
		>> document.getElementByid("txt-first-name")
*/

const spanFullName = document.querySelector("#span-full-name");

// event listeners
// event can have a shorthand of (e)
// addEventListener('stringOfAnEvent'', function)
// innerHTML - makes dynamics the content of HTML tag


const updateFullName = () =>{
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}


txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);

/*//mmultiple listeners
txtFirstName.addEventListener('keyup', (event) =>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});*/